"""
City Finder Processor
---------------------

This is an example of a Lintol processor. You can run it like so:

    python3 processor.py out-example-2021-02-01-hansard-plenary.txt

or, if you would like a nicely-formatted HTML page to look at:

    ltldoorstep -o html --output-file output.html process sample_transcripts/out-example-2021-02-01-hansard-plenary.txt processor.py -e dask.threaded

This will create output.html in the current directory and, in a browser (tested with Chrome), should look like output.png.
"""

import re
import sys
import logging
from dask.threaded import get

from ltldoorstep.processor import DoorstepProcessor
from ltldoorstep.aspect import AnnotatedTextAspect
from ltldoorstep.reports.report import combine_reports
from ltldoorstep.document_utils import load_text, split_into_paragraphs

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation

import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
import numpy as np



def get_topic_top_words(model, feature_names, no_top_words):
    """
    Utility function for getting the most frequent words used in a topic
    Returns a dictionary: keyed by topic number and values are the string of top words
    """
    topic_word_dict = {}

    for topic_idx, topic in enumerate(model.components_):
        top_words = " ".join([feature_names[i] for i in topic.argsort()[:-no_top_words - 1:-1]])
        topic_word_dict[topic_idx] = top_words

    return topic_word_dict

def topic_modelling(text, rprt, number_of_topics=8):
    """
    Add report items to determine some topics in the text and their frequency
    """

    # Ensure the text is of type string as a simple security step
    if type(text) != str:
        rprt.add_issue(
            logging.ERROR,
            'text-error',
            f'The text input was not of type string.'
        )
        return rprt

    # Splitting on "----" to divide into roughly speaker turns
    paragraphs_text = text.split("----")

    ## Step 1: Data Cleaning
    # we remove some punctuation
    paragraphs_text = [re.sub('[,\.!?\[\]\-:;]', '', x) for x in paragraphs_text]
    paragraphs_text = [paragraph for paragraph in paragraphs_text if paragraph != ""]

    # Stopwords are also removed
    other_words_to_remove = ["i", "we", "us", "me", "the"]
    paragraphs_text_clean = []
    for parag in paragraphs_text:
        parag = parag.split(" ")
        filtered_words = [word for word in parag if word not in stopwords.words('english')]
        filtered_words = [word for word in filtered_words if word.lower() not in other_words_to_remove]
        filtered_words = " ".join(filtered_words)
        paragraphs_text_clean.append(filtered_words)
    # filtered_words is a single string of the paragraph
    # paragraphs_text_clean is a list of the paragraph strings

    ## Step 2: Train Vectorizer
    # transform text from words to numerical vector
    vectorizer = CountVectorizer(token_pattern="\w+|\$[\d\.]+|\S+")
    # apply transformation
    tf = vectorizer.fit_transform(paragraphs_text_clean).toarray()
    # tf_feature_names tells us what word each column in the matric represents
    tf_feature_names = vectorizer.get_feature_names()

    ## Step 3: Prepare and Train LDA model
    model = LatentDirichletAllocation(n_components=number_of_topics, random_state=0)
    model.fit(tf)

    ## Step 4: Score all the paragraphs again to get topic frequency
    paragraph_topics = [np.argmax(model.transform(_tf.reshape(1,-1))) for _tf in tf]
    topic_counts = np.array(np.unique(paragraph_topics, return_counts=True)).T

    topic_top_word_dict = get_topic_top_words(model, tf_feature_names, 10)
    
    for topic_number, counts in topic_counts:
        rprt.add_issue(
            logging.INFO,
            'topic-counts',
            f'Found {counts} topic paragraphs comprising the vocabulary: {topic_top_word_dict[topic_number]}',
        )

    return rprt
    


class TopicModelProcessor(DoorstepProcessor):
    """
    This class wraps some of the Lintol magic under the hood, that lets us plug
    our city finder into the online version, and create reports mixing and matching
    from various processors.
    """

    # This is the type of report we create - it could be tabular (e.g. CSV), geospatial
    # (e.g. GeoJSON) or document, as in this case.
    preset = 'document'

    # This is a unique code and version to identity the processor. The code should be
    # hyphenated, lowercase, and start with lintol-code-challenge
    code = 'lintol-code-challenge-topic-modelling:1'

    # This is a short phrase or description explaining the processor.
    description = "Topic Modelling LDA for Lintol Coding Challenge"

    # Some of our processors get very complex, so this lets us build up execution graphs
    # However, for the coding challenge, you probably only want one or more steps.
    # To add two more, create functions like city_finder called town_finder and country_finder,
    # then uncomment the code in this function (and remove the extra parenthesis in the 'output' line)
    def get_workflow(self, filename, metadata={}):
        workflow = {
            'load-text': (load_text, filename),
            'get-report': (self.make_report,),
            'step-A': (topic_modelling, 'load-text', 'get-report'),
            'output': (workflow_condense, 'step-A')
        }
        return workflow

# If there are several steps, this final function pulls them into one big report.
def workflow_condense(base, *args):
    return combine_reports(*args, base=base)

# This is the actual variable Lintol looks for to set up the processor - you
# shouldn't need to touch it (except to change the class name, if neeeded)
processor = TopicModelProcessor.make

# Lintol will normally execute this processor in its own magical way, but you
# can also run it via the command line without using ltldoorstep at all (just the
# libraries already imported). The code below lets this happen, and prints out a
# JSON version of the report.
if __name__ == "__main__":
    argv = sys.argv
    processor = TopicModelProcessor()
    processor.initialize()
    workflow = processor.build_workflow(argv[1])
    print(get(workflow, 'output'))
