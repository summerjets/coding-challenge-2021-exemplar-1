# Topic Modelling Processor for NI Assembly Hansard Minutes

This is an example of a Lintol processor. You can run it like so:

    python3 processor.py out-example-2021-02-01-hansard-plenary.txt

or, if you would like a nicely-formatted HTML page to look at:

    ltldoorstep -o html --output-file output.html process sample_transcripts/out-example-2021-02-01-hansard-plenary.txt processor.py -e dask.threaded

This will create output.html in the current directory and, in a browser (tested with Chrome), should look like output.png.

If you install and run `pytest`, this will help you automate checking changes. It will run the example test function in test_processor.py.

## Topic Modelling

The NI Assembly minutes provide section/topic headings, which can be useful in extracting the proceeding of the agenda. This analysis cuts past the agenda to provide an insight into the semantic topics of discussion.

Topic Models are a form of Unsupervised Machine Learning and use statistical language models to uncover structure and patterns in text data. 

This Processor implements one popular Topic Model algorithm, LDA (Latent Dirichlet Allocation), for use on the NI Assembly texts. 

LDA can be thought of as tagging our paragraphs into abstract topics. As an unsupervised technique, the real world meaning of each topic can not be automatically determined. Fortunately, an end user can inspect the most frequently used words in each topic, to assign some overall contextualised understanding.

The frequency counts of each paragraph's topic are provided as a quick insight to how much each topic is discussed.

```A Sample Topic Output
"Found 44 topic paragraphs comprising the vocabulary: care homes staff home report pandemic residents health families it"
```

In the example above for instance, we see that 44 paragraphs were tagged as the topic denoted by the list of words. Using our real world knowledge, we can understand this as 44 parliamentary speeches which touch on the impact of the COVID19 pandemic on Care Home residents.

A sample output is available here:
https://summerjets.gitlab.io/coding-challenge-2021-exemplar-1

Further technical explanation of LDA can be found here: https://towardsdatascience.com/end-to-end-topic-modeling-in-python-latent-dirichlet-allocation-lda-35ce4ed6b3e0


## Technical Notes of Implementation

The LDA and preparation steps are noted in the code as comments. 

**Step 1: Data Cleaning**

Punctuation and stop words are removed from the text, to prepare it for the LDA model and give results more consistent to the meaning of the words, rather than the surrounding punctuation.

**Step 2: Train Vectorizer**

Machine Learning algorithms typically don't understand plain text, even when it is encoded digitally on your monitor. The text must therefore be transformed to a vector, which is just the mathematical way of saying substituting words for numbers. This substitution happens in a consistent manner (with a random seed set for reproducibility) and allows for a dictionary-style lookup of words and their corresponding assigned number. After this step, the paragraphs are sequences of numerical values. 

**Step 3: Prepare and Train LDA model**

The LDA model uses a frequentist statistical approach, which simply means, counting up how often each word appears! This removes order and reduces the text to a bag of words. 
Fitting the LDA model uncovers the patterns and structure in the data, by separating the paragraphs into n topics that minimise some loss function.

**Step 4: Score all the paragraphs again to get topic frequency**

Scoring the data back through the LDA model returns a Topic prediction for each paragraph, which enables counts of the frequency of topics.


## Future Improvements

Would you like to contribute to this open source processor, but don't know where to start? Here's a few ideas!

Currently, the number of topics is defaulted as an optional argument, `number_of_topics`, in the `topic_modelling` function definition. It would be advantageous for the Lintol platform to allow this value to be easily configured, without making code changes. As an extension of this, such functionality could be useful across other processors, where some application specific configuration is required. 

The NI Assembly text is speaker-turn separated by "----", which are used to chunk the text for topic allocation. This is highly dependent upon the source text and other separation tokens may be better suited.

The choice of cleaning stop words and tokenization could also be expanded. Another useful technique for a larger corpus would be n-grams; instead of looking at individual words, the LDA can also run across pairs or tuples (n-grams) of words. This could be interesting for pulling out terms like "Social Care", which would otherwise be treated separately and distinctly.


## Evaluation

To be eligible for submission, your processor **must** be public, MIT/Apache licensed and build an output HTML report automatically from git.
The simplest way to do this, is to fork this repository - when you commit to _master_, Gitlab will automatically start building and pushing the output report to
https://YOURACCOUNT.gitlab.io/FORKEDREPONAME . Before submitting, make 100% sure it is appearing correctly and automatically there (you can check
build progress each time you push commits by going to the "Pipelines" page on Gitlab.

You are welcome to use other platforms, as long as your code builds and runs the report with ltldoorstep, for example with Github Actions or CircleCI
(you will need to copy over from the .gitlab-ci.yml file in this repo and adjust accordingly).

## Notes

You do not have to use Python, or a specific version of Python, _provided_ that your code takes in and outputs the correct reporting schemas. The easiest way
to ensure this is to use the Python ltldoorstep libraries as shown here (it handles that automatically for you).

You are welcome to add additional open source dependencies to the requirements.txt, or additional open data to add extra reports. While we do
not expressly prohibit calling out to external services, solutions that run without hitting third-party APIs may be seen more favourably.
